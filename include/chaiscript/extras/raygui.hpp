#ifndef CHAISCRIPT_EXTRAS_RAYGUI_HPP_
#define CHAISCRIPT_EXTRAS_RAYGUI_HPP_

#include <vector>
#include <string>

#include <chaiscript/chaiscript.hpp>

#include "raygui.h"

namespace chaiscript {
  namespace extras {
    namespace raygui {

      /**
       * Struct Definitions
       */
      ModulePtr addStructs(ModulePtr m = std::make_shared<Module>()) {
        // Style property
        m->add(user_type<GuiStyleProp>(), "GuiStyleProp");
        m->add(constructor<GuiStyleProp()>(), "GuiStyleProp");
        m->add(constructor<GuiStyleProp(const GuiStyleProp &)>(), "GuiStyleProp");
        m->add(fun(&GuiStyleProp::controlId), "controlId");
        m->add(fun(&GuiStyleProp::propertyId), "propertyId");
        m->add(fun(&GuiStyleProp::propertyValue), "propertyValue");

        return m;
      }

      inline void addConst(ModulePtr m, const int val, const std::string& name)
      {
        m->add_global_const(const_var(val), name);
      }

      /**
       * enums Definitions
       */
      ModulePtr addEnums(ModulePtr m = std::make_shared<Module>()) {
        // Gui control state
        addConst(m, GUI_STATE_NORMAL, "GUI_STATE_NORMAL");
        addConst(m, GUI_STATE_FOCUSED, "GUI_STATE_FOCUSED");
        addConst(m, GUI_STATE_PRESSED, "GUI_STATE_PRESSED");
        addConst(m, GUI_STATE_DISABLED, "GUI_STATE_DISABLED");

        // Gui control text alignment
        addConst(m, GUI_TEXT_ALIGN_LEFT, "GUI_TEXT_ALIGN_LEFT");
        addConst(m, GUI_TEXT_ALIGN_CENTER, "GUI_TEXT_ALIGN_CENTER");
        addConst(m, GUI_TEXT_ALIGN_RIGHT, "GUI_TEXT_ALIGN_RIGHT");

        // Gui controls
        addConst(m, DEFAULT, "DEFAULT");
        addConst(m, LABEL, "LABEL");
        addConst(m, BUTTON, "BUTTON");
        addConst(m, TOGGLE, "TOGGLE");
        addConst(m, SLIDER, "SLIDER");
        addConst(m, PROGRESSBAR, "PROGRESSBAR");
        addConst(m, CHECKBOX, "CHECKBOX");
        addConst(m, COMBOBOX, "COMBOBOX");
        addConst(m, DROPDOWNBOX, "DROPDOWNBOX");
        addConst(m, TEXTBOX, "TEXTBOX");
        addConst(m, VALUEBOX, "VALUEBOX");
        addConst(m, SPINNER, "SPINNER");
        addConst(m, LISTVIEW, "LISTVIEW");
        addConst(m, COLORPICKER, "COLORPICKER");
        addConst(m, SCROLLBAR, "SCROLLBAR");
        addConst(m, STATUSBAR, "STATUSBAR");

        // Gui base properties for every control
        addConst(m, BORDER_COLOR_NORMAL, "BORDER_COLOR_NORMAL");
        addConst(m, BASE_COLOR_NORMAL, "BASE_COLOR_NORMAL");
        addConst(m, TEXT_COLOR_NORMAL, "TEXT_COLOR_NORMAL");
        addConst(m, BORDER_COLOR_FOCUSED, "BORDER_COLOR_FOCUSED");
        addConst(m, BASE_COLOR_FOCUSED, "BASE_COLOR_FOCUSED");
        addConst(m, TEXT_COLOR_FOCUSED, "TEXT_COLOR_FOCUSED");
        addConst(m, BORDER_COLOR_PRESSED, "BORDER_COLOR_PRESSED");
        addConst(m, BASE_COLOR_PRESSED, "BASE_COLOR_PRESSED");
        addConst(m, TEXT_COLOR_PRESSED, "TEXT_COLOR_PRESSED");
        addConst(m, BORDER_COLOR_DISABLED, "BORDER_COLOR_DISABLED");
        addConst(m, BASE_COLOR_DISABLED, "DEBASE_COLOR_DISABLEDFAULT");
        addConst(m, TEXT_COLOR_DISABLED, "TEXT_COLOR_DISABLED");
        addConst(m, BORDER_WIDTH, "BORDER_WIDTH");
        addConst(m, TEXT_PADDING, "TEXT_PADDING");
        addConst(m, TEXT_ALIGNMENT, "TEXT_ALIGNMENT");
        addConst(m, RESERVED, "RESERVED");

        // DEFAULT properties
        addConst(m, TEXT_SIZE, "TEXT_SIZE");
        addConst(m, TEXT_SPACING, "TEXT_SPACING");
        addConst(m, LINE_COLOR, "LINE_COLOR");
        addConst(m, BACKGROUND_COLOR, "BACKGROUND_COLOR");

        // Toggle / ToggleGroup
        addConst(m, GROUP_PADDING, "GROUP_PADDING");

        // Slider / SliderBar
        addConst(m, SLIDER_WIDTH, "SLIDER_WIDTH");
        addConst(m, SLIDER_PADDING, "SLIDER_PADDING");

        // ProgressBar
        addConst(m, PROGRESS_PADDING, "PROGRESS_PADDING");

        // CheckBox
        addConst(m, CHECK_PADDING, "CHECK_PADDING");

        // ComboBox
        addConst(m, COMBO_BUTTON_WIDTH, "COMBO_BUTTON_WIDTH");
        addConst(m, COMBO_BUTTON_PADDING, "COMBO_BUTTON_PADDING");

        // DropdownBox
        addConst(m, ARROW_PADDING, "ARROW_PADDING");
        addConst(m, DROPDOWN_ITEMS_PADDING, "DROPDOWN_ITEMS_PADDING");

        // TextBox / TextBoxMulti / ValueBox / Spinner
        addConst(m, TEXT_INNER_PADDING, "TEXT_INNER_PADDING");
        addConst(m, TEXT_LINES_PADDING, "TEXT_LINES_PADDING");
        addConst(m, COLOR_SELECTED_FG, "COLOR_SELECTED_FG");
        addConst(m, COLOR_SELECTED_BG, "COLOR_SELECTED_BG");

        // Spinner
        addConst(m, SPIN_BUTTON_WIDTH, "SPIN_BUTTON_WIDTH");
        addConst(m, SPIN_BUTTON_PADDING, "SPIN_BUTTON_PADDING");

        // ScrollBar
        addConst(m, ARROWS_SIZE, "ARROWS_SIZE");
        addConst(m, ARROWS_VISIBLE, "ARROWS_VISIBLE");
        addConst(m, SCROLL_SLIDER_PADDING, "SCROLL_SLIDER_PADDING");
        addConst(m, SCROLL_SLIDER_SIZE, "SCROLL_SLIDER_SIZE");
        addConst(m, SCROLL_PADDING, "SCROLL_PADDING");
        addConst(m, SCROLL_SPEED, "SCROLL_SPEED");

        // ScrollBar side
        addConst(m, SCROLLBAR_LEFT_SIDE, "SCROLLBAR_LEFT_SIDE");
        addConst(m, SCROLLBAR_RIGHT_SIDE, "SCROLLBAR_RIGHT_SIDE");

        // ListView
        addConst(m, LIST_ITEMS_HEIGHT, "LIST_ITEMS_HEIGHT");
        addConst(m, LIST_ITEMS_PADDING, "LIST_ITEMS_PADDING");
        addConst(m, SCROLLBAR_WIDTH, "SCROLLBAR_WIDTH");
        addConst(m, SCROLLBAR_SIDE, "SCROLLBAR_SIDE");

        // ColorPicker
        addConst(m, COLOR_SELECTOR_SIZE, "COLOR_SELECTOR_SIZE");
        addConst(m, HUEBAR_WIDTH, "HUEBAR_WIDTH");
        addConst(m, HUEBAR_PADDING, "HUEBAR_PADDING");
        addConst(m, HUEBAR_SELECTOR_HEIGHT, "HUEBAR_SELECTOR_HEIGHT");
        addConst(m, HUEBAR_SELECTOR_OVERFLOW, "HUEBAR_SELECTOR_OVERFLOW");

        return m;
      }

      /**
       * State modification functions
       */
      ModulePtr addStateModification(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiEnable), "GuiEnable");
        m->add(fun(&GuiDisable), "GuiDisable");
        m->add(fun(&GuiLock), "GuiLock");
        m->add(fun(&GuiUnlock), "GuiUnlock");
        m->add(fun(&GuiFade), "GuiFade");
        m->add(fun(&GuiSetState), "GuiSetState");
        m->add(fun(&GuiGetState), "GuiGetState");

        return m;
      }

      /**
       * Font set/get functions
       */
      ModulePtr addFont(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiSetFont), "GuiSetFont");
        m->add(fun(&GuiGetFont), "GuiGetFont");

        return m;
      }

      /**
       * Style set/get functions
       */
      ModulePtr addStyle(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiSetStyle), "GuiSetStyle");
        m->add(fun(&GuiGetStyle), "GuiGetStyle");

        return m;
      }

      /**
       * Tooltips set functions
       */
      ModulePtr addTooltips(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiEnableTooltip), "GuiEnableTooltip");
        m->add(fun(&GuiDisableTooltip), "GuiDisableTooltip");
        m->add(fun(&GuiSetTooltip), "GuiSetTooltip");
        m->add(fun(&GuiClearTooltip), "GuiClearTooltip");
        
        return m;
      }

      /**
       * Container/separator controls, useful for controls organization
       */
      ModulePtr addContainerControls(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiWindowBox), "GuiWindowBox");
        m->add(fun(&GuiGroupBox), "GuiGroupBox");
        m->add(fun(&GuiLine), "GuiLine");
        m->add(fun(&GuiPanel), "GuiPanel");
        m->add(fun(&GuiScrollPanel), "GuiScrollPanel");
        
        return m;
      }

      /**
       * Basic controls set
       */
      ModulePtr addBasicControls(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiLabel), "GuiLabel");
        m->add(fun(&GuiButton), "GuiButton");
        m->add(fun(&GuiLabelButton), "GuiLabelButton");
        m->add(fun(&GuiImageButtonEx), "GuiImageButtonEx");
        m->add(fun(&GuiToggle), "GuiToggle");
        m->add(fun(&GuiToggleGroup), "GuiToggleGroup");
        m->add(fun(&GuiCheckBox), "GuiCheckBox");
        m->add(fun(&GuiComboBox), "GuiComboBox");
        m->add(fun(&GuiDropdownBox), "GuiDropdownBox");
        m->add(fun(&GuiSpinner), "GuiSpinner");
        m->add(fun(&GuiValueBox), "GuiValueBox");
        m->add(fun(&GuiTextBox), "GuiTextBox");
        m->add(fun(&GuiTextBoxMulti), "GuiTextBoxMulti");
        m->add(fun(&GuiSlider), "GuiSlider");
        m->add(fun(&GuiSliderBar), "GuiSliderBar");
        m->add(fun(&GuiProgressBar), "GuiProgressBar");
        m->add(fun(&GuiStatusBar), "GuiStatusBar");
        m->add(fun(&GuiDummyRec), "GuiDummyRec");
        m->add(fun(&GuiScrollBar), "GuiScrollBar");
        m->add(fun(&GuiGrid), "GuiGrid");

        return m;
      }

      /**
       * Advance controls set
       */
      ModulePtr addAdvancedControls(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiListView), "GuiListView");
        m->add(fun(&GuiListViewEx), "GuiListViewEx");
        m->add(fun(&GuiMessageBox), "GuiMessageBox");
        m->add(fun(&GuiTextInputBox), "GuiTextInputBox");
        m->add(fun(&GuiColorPicker), "GuiColorPicker");
        m->add(fun(&GuiColorPanel), "GuiColorPanel");
        m->add(fun(&GuiColorBarAlpha), "GuiColorBarAlpha");
        m->add(fun(&GuiColorBarHue), "GuiColorBarHue");

        return m;
      }

      /**
       * Styles loading functions
       */
      ModulePtr addStylesLoading(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiLoadStyle), "GuiLoadStyle");
        m->add(fun(&GuiLoadStyleDefault), "GuiLoadStyleDefault");

        return m;
      }

      /**
       * Gui icons functionality
       */
      ModulePtr addGuiIcons(ModulePtr m = std::make_shared<Module>()) {
        m->add(fun(&GuiIconText), "GuiIconText");

#if defined(RAYGUI_SUPPORT_ICONS)
        m->add(fun(&GuiDrawIcon), "GuiDrawIcon");
        m->add(fun(&GuiGetIcons), "GuiGetIcons");
        m->add(fun(&GuiGetIconData), "GuiGetIconData");
        m->add(fun(&GuiSetIconData), "GuiSetIconData");
        m->add(fun(&GuiSetIconPixel), "GuiSetIconPixel");
        m->add(fun(&GuiClearIconPixel), "GuiClearIconPixel");
        m->add(fun(&GuiCheckIconPixel), "GuiCheckIconPixel");
#endif
        return m;
      }

      /**
       * Bootstrap a ChaiScript module with raygui support.
       */
      ModulePtr bootstrap(ModulePtr m = std::make_shared<Module>())
      {
        // Allow converting between strings and char*.
        m->add(chaiscript::type_conversion<const std::string&, const char *>([](const std::string& input) { return input.c_str(); }));
        m->add(chaiscript::type_conversion<const char *, const std::string&>([](const char* input) { return std::string(input); }));

        // structs, enums...
        addStructs(m);
        addEnums(m);

        // Add all raygui functions.
        addStateModification(m);
        addFont(m);
        addStyle(m);
        addTooltips(m);
        addContainerControls(m);
        addBasicControls(m);
        addAdvancedControls(m);
        addStylesLoading(m);
        addGuiIcons(m);

        return m;
      }
    }
  }
}

#endif /* CHAISCRIPT_EXTRAS_RAYGUI_HPP_ */
