/*******************************************************************************************
*
*   raylib-chaiscript v0.0.1 - raylib ChaiScript Launcher
*
*   DEPENDENCIES:
*
*   raylib 3.5 - This program uses latest raylib version (www.raylib.com)
*   ChaiScript >= 6.x  - http://chaiscript.com
*
*   COMPILATION (GCC):
*
*     mkdir build
*     cd build
*     cmake ..
*     make
*
*   USAGE:
*
*     Just launch your raylib .chai file from command line:
*       ./raylib-chaiscript core_basic_window.chai
*
*     or drag & drop your .chai file over raylib-chaiscript.exe
*
*
*   LICENSE: zlib/libpng
*
*   Copyright (c) 2021 Werner Smekal
*   Copyright (c) 2018 Rob Loach (@RobLoach)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
********************************************************************************************/

#include <iostream>
#include <string>

#include "chaiscript/extras/math.hpp"
#include "chaiscript/extras/string_methods.hpp"
#include "chaiscript/extras/raylib.hpp"

// add raygui library and also ask to include actual source (RAYGUI_IMPLEMENTATION)
#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "chaiscript/extras/raygui.hpp"
#undef RAYGUI_IMPLEMENTATION    // Avoid including raygui implementation again

int main(int argc, char *argv[])
{
    std::string executableName;
    std::string fileToLoad;

    switch (argc) {
        case 0:
            executableName = "raylib-chaiscript";
            fileToLoad = "main.chai";
            break;
        case 1:
            executableName = argv[0];
            fileToLoad = "main.chai";
            break;
        default:
            executableName = argv[0];
            fileToLoad = argv[1];
            break;
    }

    if (!FileExists(fileToLoad.c_str())) {
        std::cout << "Usage:" << std::endl << "    " << GetFileName(executableName.c_str()) << " myfile.chai" << std::endl << std::endl;
        std::cout << "Attempted file " << fileToLoad << " was not found." << std::endl;
        return 1;
    }

    if (!IsFileExtension(fileToLoad.c_str(), ".chai")) {
        std::cout << "Expected file to be a .chai file." << std::endl;
        return 1;
    }

    // Create the ChaiScript environment.
    chaiscript::ChaiScript chai;

    //typedef std::vector<std::string> vector_string;
    chai.add(chaiscript::bootstrap::standard_library::vector_type<std::vector<std::string> >("VectorStr"));
    chai.add(chaiscript::bootstrap::standard_library::vector_type<std::vector<int> >("VectorInt"));

    // Load and add the math extra ChaiScript module.
    auto math = chaiscript::extras::math::bootstrap();
    chai.add(math);

    // Load and add the string methods extra ChaiScript module.
    auto string_methods = chaiscript::extras::string_methods::bootstrap();
    chai.add(string_methods);

    // Load the raylib ChaiScript module.
    auto raylib = chaiscript::extras::raylib::bootstrap();

    // Add the library to the ChaiScript instance.
    chai.add(raylib);

    // Load the raylib ChaiScript module.
    auto raygui= chaiscript::extras::raygui::bootstrap();

    // Add the library to the ChaiScript instance.
    chai.add(raygui);

    // Execute the script.
    try {
      chai.eval_file(fileToLoad);
    }
    catch (const chaiscript::exception::eval_error &ee) {
      std::cout << ee.pretty_print();
      std::cout << '\n';
    }
    catch (const chaiscript::Boxed_Value &e) {
      std::cout << "Unhandled exception thrown of type " << e.get_type_info().name() << '\n';
    }
    catch (const chaiscript::exception::load_module_error &e) {
      std::cout << "Unhandled module load error\n" << e.what() << '\n';
    }
    catch (std::exception &e) {
      std::cout << "Unhandled standard exception: " << e.what() << '\n';
    }
    catch (...) {
      std::cout << "Unhandled unknown exception" << '\n';
    }

    return 0;
}
